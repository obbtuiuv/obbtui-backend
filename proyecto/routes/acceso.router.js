'use strict';
var express = require('express'),
  accesoService = require('../services/acceso.service'),
  router = express.Router();

router.post('/', accesoService.acceso);

module.exports = router;